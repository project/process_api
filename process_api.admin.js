
// Copied from filter.admin.js
(function ($) {

Drupal.behaviors.processApiStatus = {
  attach: function (context, settings) {
    $('.process-api-status-wrapper input.form-checkbox', context).once('process-api-status', function () {
      var $checkbox = $(this);
      // Retrieve the tabledrag row belonging to this processor.
      var $row = $('#' + $checkbox.attr('id').replace(/-status$/, '-weight'), context).closest('tr');
      // Retrieve the vertical tab belonging to this processor.
      var $tab = $('#' + $checkbox.attr('id').replace(/-status$/, '-settings'), context).data('verticalTab');

      // Bind click handler to this checkbox to conditionally show and hide the
      // filter's tableDrag row and vertical tab pane.
      $checkbox.bind('click.processApiUpdate', function () {
        if ($checkbox.is(':checked')) {
          $row.show();
          if ($tab) {
            $tab.tabShow().updateSummary();
          }
        }
        else {
          $row.hide();
          if ($tab) {
            $tab.tabHide().updateSummary();
          }
        }
        // Restripe table after toggling visibility of table row.
        Drupal.tableDrag['process-api-' + $checkbox.attr('id').replace(/^edit-([^-]+)-.*$/, '$1') + '-order-table'].restripeTable();
      });

      // Attach summary for configurable items (only for screen-readers).
      if ($tab) {
        $tab.fieldset.drupalSetSummary(function (tabContext) {
          return $checkbox.is(':checked') ? Drupal.t('Enabled') : Drupal.t('Disabled');
        });
      }

      // Trigger our bound click handler to update elements to initial state.
      $checkbox.triggerHandler('click.processApiUpdate');
    });
  }
};

Drupal.behaviors.processApiEditMenu = {
  attach: function (context, settings) {
    $('.process-api-edit-menu-toggle', context).click(function (e) {
      $menu = $(this).parent().find('.process-api-edit-menu');
      if ($menu.is('.collapsed')) {
    	$menu.removeClass('collapsed');
      }
      else {
    	$menu.addClass('collapsed');
      }
      return false;
    });
  }
};

})(jQuery);
