<?php

/**
 * @file
 * Service classes.
 */

/**
 * Process service for dummy process.
 */
class ProcessApiServiceDummy extends ProcessApiServiceAbstract {

  public function configurationForm(array $form, array &$form_state) {
    $form = array(
      'test' => array(
        '#type' => 'textfield',
        '#title' => t('Test'),
        '#default_value' => isset($this->server->options['custom']['test']) ? $this->server->options['custom']['test'] : NULL,
      ),
    );

    return $form;
  }
  
  public function threadConfigurationForm(ProcessApiThread $thread, array $form, array &$form_state) {
    $form = array(
      'test' => array(
        '#type' => 'textfield',
        '#title' => t('Test'),
        '#default_value' => isset($thread->options['custom']['test']) ? $thread->options['custom']['test'] : NULL,
      ),
    );
    return $form;
  }  

  public function threadProcessItem(ProcessApiThread $thread, $id, array $item, &$context) {
    return TRUE;
  }
  
  public function threadFieldsConfigurationHeaders(ProcessApiThread $thread) {
    return array(
      'boost' => t('Boost'),
      'nothing' => t('Nothing'),
      'comment' => t('Comment'),
    );
  }
  
  public function threadFieldConfigurationForm(ProcessApiThread $thread, $header_id, array $field_info, array $form, array &$form_state) {
    static $boosts = NULL;
    
    if (!$boosts) $boosts=drupal_map_assoc(array('0.1', '0.2', '0.3', '0.5', '0.8', '1.0', '2.0', '3.0', '5.0', '8.0', '13.0', '21.0'));
    
    switch ($header_id) {
      case 'comment':
        return array(
          '#type' => 'textfield',
          '#default_value' => isset($field_info['comment'])?$field_info['comment']:'',
          '#size' => 10,
          );
      case 'boost':
        return array(
          '#type' => 'select',
          '#options' => $boosts,
          '#default_value' => isset($field_info['boost'])?$field_info['boost']:1,
        );
      default:
        return array('#markup' => '&nbsp;');
    }
  }
  
  public function threadConfigurationFormValidate(ProcessApiThread $thread, array $form, array $values, array &$form_state) {
    if (isset($form['server_dependant']['entity_type'])) {
      if ($form_state['values']['entity_type']!='node') {
        form_error($form['server_dependant']['entity_type'], t('Texmining API only supports nodes entities.'));
      }
    }
  }
  
}
