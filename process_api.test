<?php

/**
 * @file
 */

/**
 * Class for testing Process API web functionality.
 */
class ProcessApiWebTest extends DrupalWebTestCase {

  protected $server_id;
  protected $thread_id;

  protected function assertText($text, $message = '', $group = 'Other') {
    return parent::assertText($text, $message ? $message : $text, $group);
  }

  protected function drupalGet($path, array $options = array(), array $headers = array()) {
    $ret = parent::drupalGet($path, $options, $headers);
    $this->assertResponse(200, t('HTTP code 200 returned.'));
    return $ret;
  }

  protected function drupalPost($path, $edit, $submit, array $options = array(), array $headers = array(), $form_html_id = NULL, $extra_post = NULL) {
    $ret = parent::drupalPost($path, $edit, $submit, $options, $headers, $form_html_id, $extra_post);
    $this->assertResponse(200, t('HTTP code 200 returned.'));
    return $ret;
  }

  public static function getInfo() {
    return array(
      'name' => 'Test Process API framework',
      'description' => 'Tests basic functions of the Process API, like creating, editing and deleting servers and threads.',
      'group' => 'Process API',
    );
  }

  public function setUp() {
    parent::setUp('entity', 'entity_metadata', 'process_api', 'process_api_test');
  }

  public function testFramework() {
    $this->drupalLogin($this->drupalCreateUser(array('administer process_api')));
    $this->insertItems();
    $this->checkOverview1();
    $this->createThread();
    $this->insertItems(5);
    $this->createServer();
    $this->checkOverview2();
    $this->enableThread();
    $this->processItems();
    $this->editServer();
    $this->clearThread();
    $this->deleteServer();
  }

  protected function insertItems($offset = 0) {
    $count = db_query('SELECT COUNT(*) FROM {process_api_test}')->fetchField();
    $this->insertItem(array(
      'id' => $offset + 1,
      'title' => 'Title 1',
      'body' => 'Body text 1.',
      'type' => 'Item',
    ));
    $this->insertItem(array(
      'id' => $offset + 2,
      'title' => 'Title 2',
      'body' => 'Body text 2.',
      'type' => 'Item',
    ));
    $this->insertItem(array(
      'id' => $offset + 3,
      'title' => 'Title 3',
      'body' => 'Body text 3.',
      'type' => 'Item',
    ));
    $this->insertItem(array(
      'id' => $offset + 4,
      'title' => 'Title 4',
      'body' => 'Body text 4.',
      'type' => 'Page',
    ));
    $this->insertItem(array(
      'id' => $offset + 5,
      'title' => 'Title 5',
      'body' => 'Body text 5.',
      'type' => 'Page',
    ));
    $count = db_query('SELECT COUNT(*) FROM {process_api_test}')->fetchField() - $count;
    $this->assertEqual($count, 5, t('@count items inserted.', array('@count' => $count)));
  }

  protected function insertItem($values) {
    $this->drupalPost('process_api_test/insert', $values, t('Save'));
  }

  protected function checkOverview1() {
    $this->drupalGet('admin/config/system/process_api');
    $this->assertText(t('There are no process servers or threads defined yet.'), t('"No servers" message is displayed.'));
  }

  protected function createThread() {
    $values = array(
      'name' => '',
      'entity_type' => '',
      'enabled' => 1,
      'description' => 'A thread used for testing.',
      'server' => '',
      'cron_limit' => 5,
    );
    $this->drupalPost('admin/config/system/process_api/add_thread', $values, t('Create thread'));
    $this->assertText(t('!name field is required.', array('!name' => t('Thread name'))));
    $this->assertText(t('!name field is required.', array('!name' => t('Entity type'))));

    $this->thread_id = $id = 'test_thread';
    $values = array(
      'name' => 'Process API test thread',
      'machine_name' => $id,
      'entity_type' => 'process_api_test',
      'enabled' => 1,
      'description' => 'A thread used for testing.',
      'server' => '',
      'cron_limit' => 5,
    );
    $this->drupalPost(NULL, $values, t('Create thread'));

    $this->assertText(t('The thread was successfully created. Please set up its workflow now.'), t('The thread was successfully created.'));
    $found = strpos($this->getUrl(), 'admin/config/system/process_api/thread/' . $id) !== FALSE;
    $this->assertTrue($found, t('Correct redirect.'));
    $thread = process_api_thread_load($id, TRUE);
    $this->assertEqual($thread->name, $values['name'], t('Name correctly inserted.'));
    $this->assertEqual($thread->entity_type, $values['entity_type'], t('Thread entity type correctly inserted.'));
    $this->assertFalse($thread->enabled, t('Status correctly inserted.'));
    $this->assertEqual($thread->description, $values['description'], t('Description correctly inserted.'));
    $this->assertNull($thread->server, t('Thread server correctly inserted.'));
    $this->assertEqual($thread->options['cron_limit'], $values['cron_limit'], t('Cron limit correctly inserted.'));

    $values = array(
      'callbacks[process_api_alter_prepare_view][status]' => 1,
      'callbacks[process_api_alter_prepare_view][weight]' => -10,
      'callbacks[process_api_alter_add_url][status]' => 1,
      'callbacks[process_api_alter_add_url][weight]' => 0,
      'callbacks[process_api_alter_add_fulltext][status]' => 1,
      'callbacks[process_api_alter_add_fulltext][weight]' => 10,
      'processors[process_api_case_ignore][status]' => 1,
      'processors[process_api_case_ignore][weight]' => 0,
      'processors[process_api_case_ignore][settings][strings]' => 0,
      'processors[process_api_tokenizer][status]' => 1,
      'processors[process_api_tokenizer][weight]' => 20,
      'processors[process_api_tokenizer][settings][spaces]' => '[^\p{L}\p{N}]',
      'processors[process_api_tokenizer][settings][ignorable]' => '[-]',
    );
    $this->drupalPost(NULL, $values, t('Save configuration'));
    $this->assertText(t("The process thread' workflow was successfully edited. All content was scheduled for re-processing so the new settings can take effect."), t('Workflow successfully edited.'));

    $this->drupalPost("admin/config/system/process_api/thread/$id/workflow", $values, t('Save configuration'));
    $this->assertText(t('No values were changed.'));

    $values = array(
      'additional[field]' => 'parent',
    );
    $this->drupalPost(NULL, $values, t('Add fields'));
    $this->assertText(t('The available fields were successfully changed.'), t('Successfully added fields.'));
    $this->assertText('Parent » ID', t('!field displayed.', array('!field' => t('Added fields are'))));

    $values = array(
      'fields[id][type]' => 'integer',
      'fields[id][boost]' => '1.0',
      'fields[id][processed]' => 1,
      'fields[title][type]' => 'text',
      'fields[title][boost]' => '5.0',
      'fields[title][processed]' => 1,
      'fields[body][type]' => 'text',
      'fields[body][boost]' => '1.0',
      'fields[body][processed]' => 1,
      'fields[type][type]' => 'string',
      'fields[type][boost]' => '1.0',
      'fields[type][processed]' => 1,
      'fields[process_api_fulltext][type]' => 'text',
      'fields[process_api_fulltext][boost]' => '1.0',
      'fields[process_api_fulltext][processed]' => 1,
      'fields[process_api_url][type]' => 'uri',
      'fields[process_api_url][boost]' => '1.0',
      'fields[process_api_url][processed]' => 1,
      'fields[parent:id][type]' => 'integer',
      'fields[parent:id][boost]' => '1.0',
      'fields[parent:id][processed]' => 1,
      'fields[parent:title][type]' => 'text',
      'fields[parent:title][boost]' => '5.0',
      'fields[parent:title][processed]' => 1,
      'fields[parent:body][type]' => 'text',
      'fields[parent:body][boost]' => '1.0',
      'fields[parent:body][processed]' => 1,
      'fields[parent:type][type]' => 'string',
      'fields[parent:type][boost]' => '1.0',
      'fields[parent:type][processed]' => 1,
    );
    $this->drupalPost(NULL, $values, t('Save changes'));
    $this->assertText(t('The processed fields were successfully changed. The thread was cleared and will have to be re-processed with the new settings.'), t('Field settings saved.'));
    $this->assertTitle('Process API test thread | Drupal', t('Correct title when viewing thread.'));
    $this->assertText('A thread used for testing.', t('!field displayed.', array('!field' => t('Description'))));
    $this->assertText('Process API test entity', t('!field displayed.', array('!field' => t('Entity type'))));
    $this->assertText(format_plural(5, '1 item per cron run.', '@count items per cron run.'), t('!field displayed.', array('!field' => t('Cron limit'))));

    $this->drupalGet("admin/config/system/process_api/thread/$id/status");
    $this->assertText(t('The thread is currently disabled.'), t('"Disabled" status displayed.'));
    $this->assertText(t('All items still need to be processed (@total total).', array('@total' => 5)), t('!field displayed.', array('!field' => t('Correct thread status'))));
  }

  protected function createServer() {
    $values = array(
      'name' => '',
      'enabled' => 1,
      'description' => 'A server used for testing.',
      'class' => '',
    );
    $this->drupalPost('admin/config/system/process_api/add_server', $values, t('Create server'));
    $this->assertText(t('!name field is required.', array('!name' => t('Server name'))));
    $this->assertText(t('!name field is required.', array('!name' => t('Service class'))));

    $this->server_id = $id = 'test_server';
    $values = array(
      'name' => 'Process API test server',
      'machine_name' => $id,
      'enabled' => 1,
      'description' => 'A server used for testing.',
      'class' => 'process_api_test_service',
    );
    $this->drupalPost(NULL, $values, t('Create server'));

    $values2 = array(
      'options[form][test]' => 'process_api_test foo bar',
    );
    $this->drupalPost(NULL, $values2, t('Create server'));

    $this->assertText(t('The server was successfully created.'));
    $found = strpos($this->getUrl(), 'admin/config/system/process_api/server/' . $id) !== FALSE;
    $this->assertTrue($found, t('Correct redirect.'));
    $server = process_api_server_load($id, TRUE);
    $this->assertEqual($server->name, $values['name'], t('Name correctly inserted.'));
    $this->assertTrue($server->enabled, t('Status correctly inserted.'));
    $this->assertEqual($server->description, $values['description'], t('Description correctly inserted.'));
    $this->assertEqual($server->class, $values['class'], t('Service class correctly inserted.'));
    $this->assertEqual($server->options['test'], $values2['options[form][test]'], t('Service options correctly inserted.'));
    $this->assertTitle('Process API test server | Drupal', t('Correct title when viewing server.'));
    $this->assertText('A server used for testing.', t('!field displayed.', array('!field' => t('Description'))));
    $this->assertText('process_api_test_service', t('!field displayed.', array('!field' => t('Service name'))));
    $this->assertText('process_api_test_service description', t('!field displayed.', array('!field' => t('Service description'))));
    $this->assertText('process_api_test foo bar', t('!field displayed.', array('!field' => t('Service options'))));
  }

  protected function checkOverview2() {
    $this->drupalGet('admin/config/system/process_api');
    $this->assertText('Process API test server', t('!field displayed.', array('!field' => t('Server'))));
    $this->assertText('Process API test thread', t('!field displayed.', array('!field' => t('Thread'))));
    $this->assertNoText(t('There are no process servers or threads defined yet.'), t('"No servers" message not displayed.'));
  }

  protected function enableThread() {
    $values = array(
      'server' => $this->server_id,
    );
    $this->drupalPost("admin/config/system/process_api/thread/{$this->thread_id}/edit", $values, t('Save settings'));
    $this->assertText(t('The process thread was successfully edited.'));
    $this->assertText('Process API test server', t('!field displayed.', array('!field' => t('Server'))));

    $this->drupalGet("admin/config/system/process_api/thread/{$this->thread_id}/enable");
    $this->assertText(t('The thread was successfully enabled.'));
  }

  protected function processItems() {
    $this->drupalGet("admin/config/system/process_api/thread/{$this->thread_id}/status");
    $this->assertText(t('The thread is currently enabled.'), t('"Enabled" status displayed.'));
    $this->assertText(t('All items still need to be processed (@total total).', array('@total' => 10)), t('!field displayed.', array('!field' => t('Correct thread status'))));
    $this->assertText(t('Process now'), t('"Process now" button found.'));
    $this->assertText(t('Clear thread'), t('"Clear thread" button found.'));
    $this->assertNoText(t('Re-process content'), t('"Re-process" button not found.'));

    $values = array(
      'limit' => 8,
    );
    $this->drupalPost(NULL, $values, t('Process now'));
    $this->assertText(t('Successfully processed @count items.', array('@count' => 7)));
    $this->assertText(t("Some items couldn't be processed. Check the logs for details."), t('Thread errors warning is displayed.'));
    $this->assertNoText(t("Couldn't process items. Check the logs for details."), t("Process error isn't displayed."));
    $this->assertText(t('About @percentage% of all items have been processed in their latest version (@processed / @total).', array('@processed' => 7, '@total' => 10, '@percentage' => 70)), t('!field displayed.', array('!field' => t('Correct thread status'))));
    $this->assertText(t('Re-processing'), t('"Re-process" button found.'));

    $values = array(
      'limit' => 1,
    );
    $this->drupalPost(NULL, $values, t('Process now'));
    $this->assertNoPattern('/' . str_replace('144', '-?\d*', t('Successfully processed @count items.', array('@count' => 144))) . '/', t('No items could be processed.'));
    $this->assertNoText(t("Some items couldn't be processed. Check the logs for details."), t("Process errors warning isn't displayed."));
    $this->assertText(t("Couldn't process items. Check the logs for details."), t('Thread error is displayed.'));

    $values = array(
      'limit' => 100,
    );
    $this->drupalPost(NULL, $values, t('Process now'));
    $this->assertText(t('Successfully processed @count items.', array('@count' => 3)));
    $this->assertNoText(t("Some items couldn't be processed. Check the logs for details."), t("Process errors warning isn't displayed."));
    $this->assertNoText(t("Couldn't process items. Check the logs for details."), t("Process error isn't displayed."));
    $this->assertText(t('All items have been processed (@processed / @total).', array('@processed' => 10, '@total' => 10)), t('!field displayed.', array('!field' => t('Correct thread status'))));
    $this->assertNoText(t('Process now'), t('"Process now" button no longer displayed.'));
  }

  protected function editServer() {
    $values = array(
      'name' => 'test-name-foo',
      'description' => 'test-description-bar',
      'options[form][test]' => 'test-test-baz',
    );
    $this->drupalPost("admin/config/system/process_api/server/{$this->server_id}/edit", $values, t('Save settings'));
    $this->assertText(t('The process server was successfully edited.'));
    $this->assertText('test-name-foo', t('!field changed.', array('!field' => t('Name'))));
    $this->assertText('test-description-bar', t('!field changed.', array('!field' => t('Description'))));
    $this->assertText('test-test-baz', t('!field changed.', array('!field' => t('Service options'))));
  }

  protected function clearThread() {
    $this->drupalPost("admin/config/system/process_api/thread/{$this->thread_id}/status", array(), t('Clear thread'));
    $this->assertText(t('The thread was successfully cleared.'));
    $this->assertText(t('All items still need to be processed (@total total).', array('@total' => 10)), t('!field displayed.', array('!field' => t('Correct thread status'))));
  }

  protected function deleteServer() {
    $this->drupalPost("admin/config/system/process_api/server/{$this->server_id}/delete", array(), t('Confirm'));
    $this->assertNoText('test-name-foo', t('Server no longer listed.'));
    $this->drupalGet("admin/config/system/process_api/thread/{$this->thread_id}/status");
    $this->assertText(t('The thread is currently disabled.'), t('The thread was disabled and removed from the server.'));
  }

}

/**
 * Class with unit tests testing small fragments of the Process API.
 *
 * Due to severe limitations for "real" unit tests, this still has to be a
 * subclass of DrupalWebTestCase.
 */
class ProcessApiUnitTest extends DrupalWebTestCase {

  protected $thread;

  protected function assertEqual($first, $second, $message = '', $group = 'Other') {
    if (is_array($first) && is_array($second)) {
      return $this->assertTrue($this->deepEquals($first, $second), $message, $group);
    }
    else {
      return parent::assertEqual($first, $second, $message, $group);
    }
  }

  protected function deepEquals($first, $second) {
    if (!is_array($first) || !is_array($second)) {
      return $first == $second;
    }
    $first  = array_merge($first);
    $second = array_merge($second);
    foreach ($first as $key => $value) {
      if (!array_key_exists($key, $second) || !$this->deepEquals($value, $second[$key])) {
        return FALSE;
      }
      unset($second[$key]);
    }
    return empty($second);
  }

  public function getInfo() {
    return array(
      'name' => 'Test Process API components',
      'description' => 'Tests some independent components of the Process API, like the processors.',
      'group' => 'Process API',
    );
  }

  public function setUp() {
    parent::setUp('entity', 'entity_metadata', 'process_api');
    $this->thread = new ProcessApiThread(array(
      'id' => 1,
      'name' => 'test',
      'enabled' => 1,
      'entity_type' => 'user',
      'options' => array(
        'fields' => array(
          'name' => array(
            'type' => 'text',
            'boost' => 1.0,
            'processed' => 1,
          ),
          'mail' => array(
            'type' => 'string',
            'boost' => 1.0,
            'processed' => 1,
          ),
          'process_api_language' => array(
            'type' => 'string',
            'boost' => 1.0,
            'processed' => 1,
          ),
        ),
      ),
    ));
  }

  public function testUnits() {
    $this->checkIgnoreCaseProcessor();
    $this->checkTokenizer();
    $this->checkHtmlFilter();
  }

  public function checkIgnoreCaseProcessor() {
    $types = process_api_field_types();
    $orig = 'Foo bar BaZ, ÄÖÜÀÁ<>»«.';
    $processed = 'foo bar baz, äöüàá<>»«.';
    $items = array(
      1 => array(
        'name' => array(
          'type' => 'text',
          'value' => $orig,
        ),
        'mail' => array(
          'type' => 'string',
          'value' => $orig,
        ),
        'process_api_language' => array(
          'type' => 'string',
          'value' => LANGUAGE_NONE,
        ),
      ),
    );

    $processor = new ProcessApiIgnoreCase($this->thread, array('strings' => FALSE));
    $tmp = $items;
    $processor->preprocessThreadItems($tmp);
    $this->assertEqual($tmp[1]['name']['value'], $processed, t('!type field was processed.', array('!type' => $types['text'])));
    $this->assertEqual($tmp[1]['mail']['value'], $orig, t("!type field wasn't processed.", array('!type' => $types['string'])));

    $processor = new ProcessApiIgnoreCase($this->thread, array('strings' => TRUE));
    $tmp = $items;
    $processor->preprocessThreadItems($tmp);
    $this->assertEqual($tmp[1]['name']['value'], $processed, t('!type field was processed.', array('!type' => $types['text'])));
    $this->assertEqual($tmp[1]['mail']['value'], $processed, t('!type field was processed.', array('!type' => $types['string'])));

  }

  public function checkTokenizer() {
    $orig = 'Foo bar1 BaZ,  La-la-la.';
    $processed1 = array(
      array(
        'value' => 'Foo',
        'score' => 1,
      ),
      array(
        'value' => 'bar1',
        'score' => 1,
      ),
      array(
        'value' => 'BaZ',
        'score' => 1,
      ),
      array(
        'value' => 'Lalala',
        'score' => 1,
      ),
    );
    $processed2 = array(
      array(
        'value' => 'Foob',
        'score' => 1,
      ),
      array(
        'value' => 'r1B',
        'score' => 1,
      ),
      array(
        'value' => 'Z,L',
        'score' => 1,
      ),
      array(
        'value' => 'l',
        'score' => 1,
      ),
      array(
        'value' => 'l',
        'score' => 1,
      ),
      array(
        'value' => '.',
        'score' => 1,
      ),
    );
    $items = array(
      1 => array(
        'name' => array(
          'type' => 'text',
          'value' => $orig,
        ),
        'process_api_language' => array(
          'type' => 'string',
          'value' => LANGUAGE_NONE,
        ),
      ),
    );

    $processor = new ProcessApiTokenizer($this->thread, array('spaces' => '[^\p{L}\p{N}]', 'ignorable' => '[-]'));
    $tmp = $items;
    $processor->preprocessThreadItems($tmp);
    $this->assertEqual($tmp[1]['name']['value'], $processed1, t('Value was correctly tokenized with default settings.'));

    $processor = new ProcessApiTokenizer($this->thread, array('spaces' => '[-a]', 'ignorable' => '\s'));
    $tmp = $items;
    $processor->preprocessThreadItems($tmp);
    $this->assertEqual($tmp[1]['name']['value'], $processed2, t('Value was correctly tokenized with custom settings.'));
  }

  public function checkHtmlFilter() {
    $orig = <<<END
This is <em lang="en" title =
"something">a test</em>.
How to write <strong>links to <em>other sites</em></strong>: &lt;a href="URL" title="MOUSEOVER TEXT"&gt;TEXT&lt;/a&gt;.
&lt; signs can be <A HREF="http://example.com/topic/html-escapes" TITLE =  'HTML &quot;escapes&quot;'
TARGET = '_blank'>escaped</A> with "&amp;lt;".
<img src = "foo.png" alt = "someone's image" />
END;
    $tags = <<<END
em = 1.5
strong = 2
END;
    $processed1 = array(
      array('value' => 'This', 'score' => 1),
      array('value' => 'is', 'score' => 1),
      array('value' => 'something', 'score' => 1.5),
      array('value' => 'a', 'score' => 1.5),
      array('value' => 'test', 'score' => 1.5),
      array('value' => 'How', 'score' => 1),
      array('value' => 'to', 'score' => 1),
      array('value' => 'write', 'score' => 1),
      array('value' => 'links', 'score' => 2),
      array('value' => 'to', 'score' => 2),
      array('value' => 'other', 'score' => 3),
      array('value' => 'sites', 'score' => 3),
      array('value' => '<a', 'score' => 1),
      array('value' => 'href="URL"', 'score' => 1),
      array('value' => 'title="MOUSEOVER', 'score' => 1),
      array('value' => 'TEXT">TEXT</a>', 'score' => 1),
      array('value' => '<', 'score' => 1),
      array('value' => 'signs', 'score' => 1),
      array('value' => 'can', 'score' => 1),
      array('value' => 'be', 'score' => 1),
      array('value' => 'HTML', 'score' => 1),
      array('value' => '"escapes"', 'score' => 1),
      array('value' => 'escaped', 'score' => 1),
      array('value' => 'with', 'score' => 1),
      array('value' => '"&lt;"', 'score' => 1),
      array('value' => 'someone\'s', 'score' => 1),
      array('value' => 'image', 'score' => 1),
    );
    $items = array(
      1 => array(
        'name' => array(
          'type' => 'text',
          'value' => $orig,
        ),
        'process_api_language' => array(
          'type' => 'string',
          'value' => LANGUAGE_NONE,
        ),
      ),
    );

    $tmp = $items;
    $processor = new ProcessApiHtmlFilter($this->thread, array('title' => TRUE, 'alt' => TRUE, 'tags' => $tags));
    $processor->preprocessThreadItems($tmp);
    $processor = new ProcessApiTokenizer($this->thread, array('spaces' => '[\s.:]', 'ignorable' => ''));
    $processor->preprocessThreadItems($tmp);
    $this->assertEqual($tmp[1]['name']['value'], $processed1, t('Text was correctly processed.'));
  }

}
