<?php

/**
 * @file
 * Hooks provided by the Process API module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Defines one or more process service classes a module offers.
 *
 * Note: The ids should be valid PHP identifiers.
 *
 * @see hook_process_api_service_info_alter()
 *
 * @return array
 *   An associative array of process service classes, keyed by a unique
 *   identifier and containing associative arrays with the following keys:
 *   - name: The service class' translated name.
 *   - description: A translated string to be shown to administrators when
 *     selecting a service class. Should contain all peculiarities of the
 *     service class, like field type support, supported features (like facets),
 *     the "direct" parse mode and other specific things to keep in mind.
 *   - class: The service class, which has to implement the
 *     ProcessApiServiceInterface interface.
 */
function hook_process_api_service_info() {
  $services['example_some'] = array(
    'name' => t('Some Service'),
    'description' => t('Service for some process engine.'),
    'class' => 'SomeServiceClass',
    // Unknown keys can later be read by the object for additional information.
    'init args' => array('foo' => 'Foo', 'bar' => 42),
  );
  $services['example_other'] = array(
    'name' => t('Other Service'),
    'description' => t('Service for another process engine.'),
    'class' => 'OtherServiceClass',
  );

  return $services;
}

/**
 * Alter the Process API service info.
 *
 * Modules may implement this hook to alter the information that defines Process
 * API service. All properties that are available in
 * hook_process_api_service_info() can be altered here.
 *
 * @see hook_process_api_service_info()
 *
 * @param array $service_info
 *   The Process API service info array, keyed by service id.
 */
function hook_process_api_service_info_alter(array &$service_info) {
  foreach ($service_info as $id => $info) {
    $service_info[$id]['class'] = 'MyProxyServiceClass';
    $service_info[$id]['example_original_class'] = $info['class'];
  }
}

/**
 * Registers one or more callbacks that can be called at process time to add
 * additional data to the processed items (e.g. comments or attachments to nodes),
 * alter the data in other forms or remove items from the array.
 *
 * For the required signature of callbacks, see example_random_alter().
 *
 * @see example_random_alter()
 *
 * @return array
 *   An associative array keyed by the function names and containing
 *   arrays with the following keys:
 *   - name: The name to display for this callback.
 *   - description: A short description of what the callback does.
 *   - weight: (optional) Defines the order in which callbacks are displayed
 *     (and, therefore, invoked) by default. Defaults to 0.
 */
function hook_process_api_alter_callback_info() {
  $callbacks['example_random_alter'] = array(
    'name' => t('Random alteration'),
    'description' => t('Alters all passed item data completely randomly.'),
    'weight' => 100,
  );
  $callbacks['example_add_comments'] = array(
    'name' => t('Add comments'),
    'description' => t('For nodes and similar entities, adds comments.'),
  );

  return $callbacks;
}

/**
 * Registers one or more processors. These are classes implementing the
 * ProcessApiProcessorInterface interface which can be used at process
 * time to pre-process item data.
 *
 * @see ProcessApiProcessorInterface
 *
 * @return array
 *   An associative array keyed by the processor id and containing arrays
 *   with the following keys:
 *   - name: The name to display for this processor.
 *   - description: A short description of what the processor does at each
 *     phase.
 *   - class: The processor class, which has to implement the
 *     ProcessApiProcessorInterface interface.
 *   - weight: (optional) Defines the order in which processors are displayed
 *     (and, therefore, invoked) by default. Defaults to 0.
 */
function hook_process_api_processor_info() {
  $callbacks['example_processor'] = array(
    'name' => t('Example processor'),
    'description' => t('Pre- and post-processes data in really cool ways.'),
    'class' => 'ExampleProcessApiProcessor',
    'weight' => -1,
  );
  $callbacks['example_processor_minimal'] = array(
    'name' => t('Example processor 2'),
    'description' => t('Processor with minimal description.'),
    'class' => 'ExampleProcessApiProcessor2',
  );

  return $callbacks;
}

/**
 * Act on process servers when they are loaded.
 *
 * @param array $servers
 *   An array of loaded ProcessApiServer objects.
 */
function hook_process_api_server_load(array $servers) {
  foreach ($servers as $server) {
    db_insert('example_process_server_access')
      ->fields(array(
        'server' => $server->machine_name,
        'access_time' => REQUEST_TIME,
      ))
      ->execute();
  }
}

/**
 * A new process server was created.
 *
 * @param ProcessApiServer $server
 *   The new server.
 */
function hook_process_api_server_insert(ProcessApiServer $server) {
  db_insert('example_process_server')
    ->fields(array(
      'server' => $server->machine_name,
      'insert_time' => REQUEST_TIME,
    ))
    ->execute();
}

/**
 * A process server was edited, enabled or disabled.
 *
 * @param ProcessApiServer $server
 *   The edited server.
 * @param $op
 *   A hint as to how the server was updated. Either 'enable', 'disable' or
 *   'edit'.
 */
function hook_process_api_server_update(ProcessApiServer $server, $op = 'edit') {
  db_insert('example_process_server_update')
    ->fields(array(
      'server' => $server->machine_name,
      'update_time' => REQUEST_TIME,
    ))
    ->execute();
}

/**
 * A process server was deleted.
 *
 * @param ProcessApiServer $server
 *   The deleted server.
 */
function hook_process_api_server_delete(ProcessApiServer $server) {
  db_insert('example_process_server_update')
    ->fields(array(
      'server' => $server->machine_name,
      'update_time' => REQUEST_TIME,
    ))
    ->execute();
  db_delete('example_process_server')
    ->condition('server', $server->machine_name)
    ->execute();
}

/**
 * Act on process threades when they are loaded.
 *
 * @param array $threads
 *   An array of loaded ProcessApiThread objects.
 */
function hook_process_api_thread_load(array $threads) {
  foreach ($threads as $thread) {
    db_insert('example_process_thread_access')
      ->fields(array(
        'thread' => $thread->machine_name,
        'access_time' => REQUEST_TIME,
      ))
      ->execute();
  }
}

/**
 * A new process thread was created.
 *
 * @param ProcessApiThread $thread
 *   The new thread.
 */
function hook_process_api_thread_insert(ProcessApiThread $thread) {
  db_insert('example_process_thread')
    ->fields(array(
      'thread' => $thread->machine_name,
      'insert_time' => REQUEST_TIME,
    ))
    ->execute();
}

/**
 * A process thread was edited in any way.
 *
 * This includes being edited, enabled or disabled, as well as the thread being
 * cleared or scheduled for re-processing.
 *
 * @param ProcessApiThread $thread
 *   The edited thread.
 * @param $op
 *   A hint as to how the thread was updated. Either 'enable', 'disable', 'edit',
 *   'reprocess', 'clear' or 'fields'.
 */
function hook_process_api_thread_update(ProcessApiThread $thread, $op = 'edit') {
  db_insert('example_process_thread_update')
    ->fields(array(
      'thread' => $thread->machine_name,
      'update_time' => REQUEST_TIME,
    ))
    ->execute();
}

/**
 * A process thread was deleted.
 *
 * @param ProcessApiThread $thread
 *   The deleted thread.
 */
function hook_process_api_thread_delete(ProcessApiThread $thread) {
  db_insert('example_process_thread_update')
    ->fields(array(
      'thread' => $thread->machine_name,
      'update_time' => REQUEST_TIME,
    ))
    ->execute();
  db_delete('example_process_thread')
    ->condition('thread', $thread->machine_name)
    ->execute();
}

/**
 * @} End of "addtogroup hooks".
 */

/**
 * Process API data alteration callback that randomly changes item data.
 *
 * @param ProcessApiThread $thread
 *   The thread on which the items are processed.
 * @param array $items
 *   An array of objects containing the entity data.
 *
 * @return array
 *   An array of property infos for all added properties, as required by
 *   hook_entity_property_info().
 */
function example_random_alter(ProcessApiThread $thread, array &$items) {
  if ($thread->id % 2) {
    foreach ($items as $id => $item) {
      srand($id);
      if (rand(0, 4)) {
        unset($items[$id]);
        continue;
      }
      foreach ($item as $k => $v) {
        srand(drupal_strlen($v) + count($v));
        $item->$k = rand(1, 100);
      }
    }
  }

  foreach ($items as $id => $item) {
    $item->example_id_plus_rand = "$id-" . rand(1, 500);
  }

  return array(
    'example_id_plus_rand' => array(
      'label' => t('Useless field'),
      'description' => t('A really, really useless field, consisting of the entity ID and a random number.'),
      'type' => 'text',
    )
  );
}
