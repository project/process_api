
Process API
-----------

Process API is a pluggable framework module that handle processing of "dirty" entities (aka new or modified entities since last process).
You can define process on every entity or "related entities package".
Process can be synchronous or asynchronous relative to entity events.
UI can be altered to reflect your needs.


Terminology
-----------

- Process service : it handles the behaviour of your process. A module providing a service must implements a class with the ProcessApiServiceInterface. It could also extend the ProcessApiServiceAbstract.
- Process server : an entity to handle a "server" that provides a service. Server's settings are extendable through ProcessApiServiceInterface.
- Process thread : an entity to handle a "thread". A thread links a server to a main entity with related fields and/or entities. The thread keeps track of entities that need (re)processing.


Workflow
--------
Process API catch entity modification in hook_entity_update() and hook_entity_insert() and flag it to be processed.
Process can be postponed to cron or done "live".


Plugin
------
Plugins are module that implements hook_process_api_service_info() and provide a Process API service class extending ProcessApiServiceAbstract.
ProcessApiServiceAbstract is a basic implementation of ProcessApiServiceInterface.
At least you have to implement ProcessApiServiceInterface::threadProcessItem().

See contrib/process_api_dummy : a basic dummy process service