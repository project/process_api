<?php

/**
 * @file
 * Exception classes.
 */

/**
 * Represents an exception or error that occurred in some part of the Process API
 * framework.
 */
class ProcessApiException extends Exception {

  /**
   * Creates a new ProcessApiException.
   *
   * @param $message
   *   A string describing the cause of the exception.
   */
  public function __construct($message = NULL) {
    if (!$message) {
      $message = t('An error occcurred in the Process API framework.');
    }
    parent::__construct($message);
  }

}
