<?php

/**
 * @file
 * Entity classes.
 */

class ProcessApiEntity extends Entity {
  
  /**
   * Array of fields to snapshot and/or compare with optional callback functions.
   * @see applyChangeTriggers()
   * @see snapshot()
   * 
   *   array(
   *     field => array(
   *       'snapshot' => 'mySnapshot', // [optional] function to get the field value : mySnapshot($value), return the field value
   *       'compare' => 'myCompare', // [optional] compare function : myCompare($old, $new), returns 0 if equal, uses !== otherwise
   *       'trigger' => 'myTrigger', // [optional, can be just to store the value] triggered function : myTrigger($old, $new, $tag), if returns TRUE the trigger is flagged as done
   *     ) 
   *   )
   *   
   * If a callback is an array with the first element set to NULL, we set it to $this, see call_user_func().
   * @see call_user_func()
   * 
   * @var array
   */
  protected $_changeTriggers = array();
  
  /**
   * Field value snapshots.
   * @see applyChangeTriggers()
   * @see snapshot()
   * 
   * @var array
   */
  protected $_snapshots = array();
  
  public function setValues(array $fields) {
    foreach ($fields as $field => $value) {
      $this->$field=$value;
    }
  }
  
  /**
   * Take a snapshot of fields you want to keep track so the system can trigger event on field change.
   * @param array|string|NULL $fields snapshot given fields, all if NULL
   * 
   * @see $_changeTriggers
   * @see applyChangeTriggers()
   */
  public function snapshot($fields = NULL) {
    //$this->_snapshots=array(); TODO is it safe to remove ?
    if ($fields) {
      if (!is_array($fields)) {
        $fields=array($fields);
      }
      $triggers=array_intersect_key($this->_changeTriggers, array_combine($fields, $fields));
    }
    else {
      $triggers=$this->_changeTriggers;
    }
    
    foreach ($triggers as $field => &$trigger) {
      if (isset($trigger['snapshot'])) {
        if (is_array($trigger['snapshot'])&&$trigger['snapshot'][0]===NULL) {
          $trigger['snapshot'][0]=$this;
        }
        $this->_snapshots[$field]=call_user_func($trigger['snapshot'], $this->$field);
      }
      else {
        $this->_snapshots[$field]=$this->$field;
      }
    }
  }
  
  /**
   * Test and apply trigger.
   * @see $_changeTriggers
   * @throws ProcessApiException
   */
  public function applyChangeTriggers($tag=NULL) {
    if (!count($this->_changeTriggers)) return;
    foreach ($this->_changeTriggers as $field => &$trigger) {
      if (isset($trigger['trigger'])&&isset($this->_snapshots[$field])) {
        if (isset($trigger['compare'])) {
          if (is_array($trigger['compare'])&&$trigger['compare'][0]===NULL) {
            $trigger['compare'][0]=$this;
          }
          $res=call_user_func($trigger['compare'], $this->_snapshots[$field], $this->$field);
        }
        else {
          $res=($this->$field!==$this->_snapshots[$field]);
        }
        if ($res) {
           
          if (is_array($trigger['trigger'])&&$trigger['trigger'][0]===NULL) {
            $trigger['trigger'][0]=$this;
          }
          if (call_user_func($trigger['trigger'], $this->_snapshots[$field], $this->$field, $tag)) {
            // trigger done
            unset($this->_snapshots[$field]);
          }
        }   
      }
    }
  }
  
  public function __sleep() {
    return array_diff(array_keys(get_object_vars($this)), array('_changeTriggers', '_snapshots'));
  }
}

/**
 * Class representing a process server.
 *
 * This can handle the same calls as defined in the ProcessApiServiceInterface
 * and pass it on to the service implementation appropriate for this server.
 */
class ProcessApiServer extends ProcessApiEntity {

  /* Database values that will be set when object is loaded: */

  /**
   * @var integer
   */
  public $id = 0;

  /**
   * @var string
   */
  public $name = '';

  /**
   * @var string
   */
  public $machine_name = '';

  /**
   * @var string
   */
  public $description = '';

  /**
   * @var string
   */
  public $class = '';

  /**
   * @var array
   */
  public $options = array();

  /**
   * @var integer
   */
  public $enabled = 1;


  /**
   * Will be set by the Entity CRUD API for exporting.
   */
  //public $module;
  /**
   * Will be set by the Entity CRUD API for exporting.
   */
  //public $status;

  
  /**
   * Proxy object for invoking service methods.
   *
   * @var ProcessApiServiceInterface
   */
  protected $proxy;

  /**
   * Constructor as a helper to the parent constructor.
   */
  public function __construct(array $values = array()) {
    parent::__construct($values, 'process_api_server');
  }

  /**
   * Saves this server to the database, either creating a new record or updating
   * an existing one.
   *
   * @param $op
   *   If the server is not newly inserted, this will be passed to
   *   hook_process_api_server_update().
   *
   * @return
   *   Failure to save the server will return FALSE. Otherwise, SAVED_NEW or
   *   SAVED_UPDATED is returned depending on the operation performed. $this->id
   *   will be set if a new server was inserted.
   */
  public function save($op = 'edit') {
    foreach (array('name', 'machine_name', 'class', 'options') as $var) {
      if (empty($this->$var)) {
        throw new ProcessApiException(t('Property !field has to be set for a server!', array('!field' => $var)));
      }
    }
    if (empty($this->description)) {
      $this->description = NULL;
    }

    $ret = parent::save();
    if ($ret == SAVED_NEW) {
      $this->postCreate();
    }
    elseif ($ret == SAVED_UPDATED && $op != 'edit') {
      module_invoke_all('process_api_server_update', $this, $op);
    }

    return $ret;
  }

  /**
   * Wrapper for save() to correctly call postUpdate().
   *
   * NOTE: You shouldn't change any properties of this object before calling
   * this method, as this might lead to the fields not being saved correctly.
   *
   * @param array $fields
   *   The new field values. $this->enabled cannot be set this way, use
   *   $this->enable() instead.
   *
   * @return
   *   SAVE_UPDATED on success, FALSE on failure, 0 if the fields already had
   *   the specified values.
   */
  public function update(array $fields) {
    $changeable = array('name' => 1, 'description' => 1, 'options' => 1);
    foreach ($fields as $field => $value) {
      if (!isset($changeable[$field]) || $value === $this->$field) {
        unset($fields[$field]);
      }
    }
    // If there are no new values, just return 0.
    if (empty($fields)) {
      return 0;
    }
    $reprocess = $this->postUpdate($fields);
    foreach ($fields as $field => $value) {
      $this->$field = $value;
    }
    $ret = $this->save('edit');
    if ($ret) {
      if ($reprocess) {
        foreach (process_api_thread_load_multiple(FALSE, array('server' => $this->machine_name, 'enabled' => TRUE), FALSE, TRUE) as $thread) {
          $thread->reprocess();
        }
      }
    }
    return $ret;
  }

  /**
   * Enables or disables this server.
   *
   * @param $enabled
   *   The new enabled status of the server.
   *
   * @return
   *   SAVE_UPDATED on success, FALSE on failure, 0 if the field already had
   *   the specified value.
   */
  public function enable($enabled = TRUE) {
    if ($this->enabled == $enabled) {
      return 0;
    }
    $this->enabled = $enabled;
    $ret = $this->save($enabled ? 'enable' : 'disable');
    if (!$ret) {
      return $ret;
    }

    if ($enabled) {
      $this->postEnable();
      // Were there any changes in the server's threads while it was disabled?
      $tasks = variable_get('process_api_tasks', array());
      if (isset($tasks[$this->machine_name])) {
        foreach ($tasks[$this->machine_name] as $thread_id => $thread_tasks) {
          $thread = process_api_thread_load($thread_id);
          foreach ($thread_tasks as $task) {
            switch ($task) {
              case 'add':
                $this->addThread($thread);
                break;
              case 'clear':
                $this->deleteItems('all', $thread);
                break;
              case 'clear all':
                // Would normally be used with a fake thread ID of '', since it doesn't matter.
                $this->deleteItems('all');
                break;
              case 'fields':
                if ($this->fieldsUpdated($thread)) {
                  _process_api_thread_reprocess($thread->machine_name);
                }
                break;
              case 'remove':
                $this->removeThread($thread ? $thread : $thread_id);
                break;
              default:
                if (substr($task, 0, 7) == 'delete-') {
                  $id = substr($task, 7);
                  $this->deleteItems(array($id), $thread);
                }
                else {
                  watchdog('process_api', 'Unknown task "!task" for server "!name".', array('!task' => $task, '!name' => $this->machine_name), 'warning');
                }
            }
          }
        }
        unset($tasks[$this->machine_name]);
        variable_set('process_api_tasks', $tasks);
      }
    }
    else {
      $this->postDisable();
      foreach (process_api_thread_load_multiple(FALSE, array('server' => $this->machine_name), FALSE, TRUE) as $thread) {
        $thread->enable(FALSE);
      }
    }

    return $ret;
  }

  /**
   * Deletes this server.
   *
   * @return
   *   1 on success, 0 or FALSE on failure.
   */
  public function delete() {
    // Call hook method before doing anything else.
    $this->preDelete();

    foreach (process_api_thread_load_multiple(FALSE, array('server' => $this->machine_name), FALSE, TRUE) as $thread) {
      $thread->delete();
    }

    parent::delete();

    $tasks = variable_get('process_api_tasks', array());
    unset($tasks[$this->machine_name]);
    variable_set('process_api_tasks', $tasks);

    return 1;
  }

  /**
   * Magic method for determining which fields should be serialized.
   *
   * Serialize all properties except the proxy object.
   *
   * @return array
   *   An array of properties to be serialized.
   */
  public function __sleep() {
    $ret = parent::__sleep();
    foreach (array('id', 'proxy', 'status', 'is_new') as $field) {
      $i=array_search($field, $ret);
      if ($i!==FALSE) unset($ret[$i]);
    }
    return $ret;
  }

  /**
   * Helper method for ensuring the proxy object is set up.
   * @return ProcessApiServiceInterface
   */
  public function ensureProxy() {
    if (!isset($this->proxy)) {
      $info = process_api_get_service_info($this->class);
      if ($info && class_exists($info['class'])) {
        if (empty($this->options)) {
          // We always have to provide the options.
          $this->options = array();
        }
        $this->proxy = new $info['class']($this);
      }
      if (!($this->proxy instanceof ProcessApiServiceInterface)) {
        throw new ProcessApiException(t('Process server with machine name !name specifies illegal service class !class.', array('!name' => $this->machine_name, '!class' => $this->class)));
      }
    }
    return  $this->proxy;
  }
  
  /**
   * If the service class defines additional methods, not specified in the
   * ProcessApiServiceInterface interface, then they are called via this magic
   * method.
   */
  public function __call($name, $arguments = array()) {
    $this->ensureProxy();
    return call_user_func_array(array($this->proxy, $name), $arguments);
  }

  // Proxy methods

  // For increased clarity, and since some parameters are passed by reference,
  // we don't use the __call() magic method for those.

  public function configurationForm(array $form, array &$form_state) {
    $this->ensureProxy();
    return $this->proxy->configurationForm($form, $form_state);
  }

  public function configurationFormValidate(array $form, array $values, array &$form_state) {
    $this->ensureProxy();
    return $this->proxy->configurationFormValidate($form, $values, $form_state);
  }

  public function configurationFormSubmit(array $form, array $values, array &$form_state) {
    $this->ensureProxy();
    return $this->proxy->configurationFormSubmit($form, $values, $form_state);
  }

  public function supportsFeature($feature) {
    $this->ensureProxy();
    return $this->proxy->supportsFeature($feature);
  }

  public function viewSettings() {
    $this->ensureProxy();
    return $this->proxy->viewSettings();
  }

  public function postCreate() {
    $this->ensureProxy();
    return $this->proxy->postCreate();
  }

  public function postUpdate(array $fields) {
    $this->ensureProxy();
    return $this->proxy->postUpdate($fields);
  }

  public function postEnable() {
    $this->ensureProxy();
    return $this->proxy->postEnable();
  }

  public function postDisable() {
    $this->ensureProxy();
    return $this->proxy->postDisable();
  }

  public function preDelete() {
    $this->ensureProxy();
    return $this->proxy->preDelete();
  }

  public function addThread(ProcessApiThread $thread) {
    $this->ensureProxy();
    return $this->proxy->addThread($thread);
  }

  public function fieldsUpdated(ProcessApiThread $thread) {
    $this->ensureProxy();
    return $this->proxy->fieldsUpdated($thread);
  }

  public function removeThread($thread) {
    $this->ensureProxy();
    return $this->proxy->removeThread($thread);
  }


  public function deleteItems($ids = 'all', ProcessApiThread $thread = NULL) {
    $this->ensureProxy();
    return $this->proxy->deleteItems($ids, $thread);
  }

}

/**
 * Class representing a process thread.
 */
class ProcessApiThread extends ProcessApiEntity {

  /**
   * Proxy object for invoking service methods.
   *
   * @var ProcessApiServiceInterface
   */
  protected $proxy;
  
  /**
   * @var ProcessApiServer
   */
  protected $server_object = NULL;

  /**
   * @var array
   */
  protected $processors = NULL;

  /**
   * @var array
   */
  protected $added_properties = array();

  /**
   * @var array
   */
  protected $fulltext_fields = array();

  // Database values that will be set when object is loaded

  /**
   * @var integer
   */
  public $id;

  /**
   * @var string
   */
  public $name;

  /**
   * @var integer
   */
  public $weight;
  
  /**
   * @var string
   */
  public $machine_name;

  /**
   * @var string
   */
  public $description;

  /**
   * @var string
   */
  public $server;

  /**
   * @var string
   */
  public $entity_type;

  /**
   * @var array
   */
  public $options  = array();

  /**
   * @var integer
   */
  public $enabled;

  /**
   * Reprocess flag stack.
   * @var array
   */
  protected $_need_reprocess=array();
  
  protected $_changeTriggers = array(
    'server' => array('trigger' => array(NULL, 'changeTriggerServer')),
    'options' => array('trigger' => array(NULL, 'changeTriggerOptions')),
  );
  
  /**
   * Constructor as a helper to the parent constructor.
   */
  public function __construct(array $values = array()) {
    parent::__construct($values, 'process_api_thread');
  }

  /**
   * Execute necessary tasks for a newly created thread (either created in the
   * database, or for the first time loaded from code).
   */
  public function postCreate() {
    // Remember items to process.
    $entity_info = entity_get_info($this->entity_type);
    if (!empty($entity_info['base table'])) {
      // Use a subselect, which will probably be much faster than entity_load().
      // We just assume that no module/entity type will be stupid enough to use "base table" and
      // "entity_keys[id]" in a different way than the default controller.
      $id_field = $entity_info['entity keys']['id'];
      $table = $entity_info['base table'];
      $query = db_select($table, 't');
      $query->addField('t', $id_field, 'item_id');
      $query->addExpression(':thread_id', 'thread_id', array(':thread_id' => $this->machine_name));
      $query->addExpression('1', 'changed');

      db_insert('process_api_item')->from($query)->execute();
    }
    else {
      // We have to use the slow entity_load().
      $entities = entity_load($this->entity_type, FALSE);
      $query = db_insert('process_api_item')->fields(array('item_id', 'thread_id', 'changed'));
      foreach ($entities as $item_id => $entity) {
        $query->values(array(
          'item_id' => $item_id,
          'thread_id' => $this->machine_name,
          'changed' => 1,
        ));
      }
      $query->execute();
    }

    $server = $this->server();
    if ($server) {
      // Tell the server about the new thread.
      if ($server->enabled) {
        $server->addThread($this);
      }
      else {
        $tasks = variable_get('process_api_tasks', array());
        // When we add or remove a thread, we can ignore all other tasks.
        $tasks[$server->machine_name][$this->machine_name] = array('add');
        variable_set('process_api_tasks', $tasks);
      }
    }
  }

  /**
   * Execute necessary tasks when thread is either deleted from the database or
   * not defined in code anymore.
   */
  public function postDelete() {
    if ($server = $this->server()) {
      if ($server->enabled) {
        $server->removeThread($this);
      }
      else {
        $tasks = variable_get('process_api_tasks', array());
        $tasks[$server->machine_name][$this->machine_name] = array('remove');
        variable_set('process_api_tasks', $tasks);
      }
    }

    db_delete('process_api_item')
      ->condition('thread_id', $this->machine_name)
      ->execute();
  }

  /**
   * Saves this thread to the database, either creating a new record or updating
   * an existing one.
   *
   * @param $op
   *   If the thread is not newly inserted, this will be passed to
   *   hook_process_api_thread_update().
   *
   * @return
   *   Failure to save the thread will return FALSE. Otherwise, SAVED_NEW or
   *   SAVED_UPDATED is returned depending on the operation performed. $this->id
   *   will be set if a new thread was inserted.
   */
  public function save($op = 'edit') {
    foreach (array('name', 'machine_name', 'entity_type', 'options') as $var) {
      if (empty($this->$var)) {
        throw new ProcessApiException(t('Property !field has to be set for a thread!', array('!field' => $var)));
      }
    }
    if (empty($this->description)) {
      $this->description = NULL;
    }
    if (empty($this->server)) {
      $this->server = NULL;
      $this->enabled = FALSE;
    }
    // This will also throw an exception if the server doesn't exist – which is good.
    elseif (!$this->server()->enabled) {
      $this->enabled = FALSE;
    }

    $ret = parent::save();
    // We only have to execute the "new thread" tasks when the entity was newly created, not overridden.
    if ($ret == SAVED_NEW && (!isset($this->status) || !($this->status & ENTITY_IN_CODE))) {
      $this->postCreate();
    }
    elseif ($ret == SAVED_UPDATED && $op != 'edit') {
      module_invoke_all('process_api_thread_update', $this, $op);
    }

    return $ret;
  }

  /**
   * Reprocess flag stack.
   * 
   * @param string $need
   */
  public function needReprocess($need = NULL) {
    if ($need !== NULL) {
      $this->_need_reprocess[$need]=$need;
    }
    return $this->_need_reprocess;
  }
  
  /**
   * Wrapper for save() to correctly call hooks.
   *
   * NOTE: You shouldn't change any properties of this object before calling
   * this method, as this might lead to the fields not being saved correctly.
   *
   *
   * @return
   *   SAVE_UPDATED on success, FALSE on failure, 0 if the fields already had
   *   the specified values.
   */
  public function update() {
    
    $this->applyChangeTriggers('update');
    
    $ret = parent::save('edit');

    if ($ret) {
      $this->applyChangeTriggers('post_update');
    }
    
    if ($ops=$this->needReprocess()) {
      $this->reprocess($ops);
    }

    return $ret;
  }

  /**
   * Enables or disables this thread.
   *
   * @param $enabled
   *   The new enabled status of the thread.
   *
   * @throws ProcessApiException
   *   If $enabled is TRUE and the thread' server isn't enabled.
   *
   * @return
   *   SAVE_UPDATED on success, FALSE on failure, 0 if the field already had
   *   the specified value.
   */
  public function enable($enabled = TRUE) {
    if ($this->enabled == $enabled) {
      return 0;
    }
    if ($enabled && !($this->server() && $this->server()->enabled)) {
      throw new ProcessApiException(t("Attempt to enable thread !name which isn't on an enabled server.", array('!name' => $this->machine_name)));
    }
    $this->enabled = $enabled;
    return $this->save($enabled ? 'enable' : 'disable');
  }

  /**
   * Schedules this process thread for re-processing.
   *
   * @return
   *   TRUE on success, FALSE on failure.
   */
  public function reprocess($ops = 'reprocess') {
    if (!is_array($ops)) {
      $ops=array($ops);
    }
    foreach ($ops as $op) {
      if (isset($this->_need_reprocess[$op])) {
        unset($this->_need_reprocess[$op]);
      }
    }
    if (!$this->server) {
      return TRUE;
    }
    
    $ret = _process_api_thread_reprocess($this->machine_name);
    if ($ret) {
      module_invoke_all('process_api_thread_update', $this, $ops);
    }
    return TRUE;
  }

  /**
   * Clears this process thread and schedules all of its items for re-processing.
   *
   * @return
   *   TRUE on success, FALSE on failure.
   */
  public function clear() {
    if (!$this->server) {
      return TRUE;
    }

    $server = $this->server();
    if ($server->enabled) {
      $server->deleteItems('all', $this);
    }
    else {
      $tasks = variable_get('process_api_tasks', array());
      // If the thread was cleared or newly added since the server was last enabled, we don't need to do anything.
      if (!isset($tasks[$server->machine_name][$id])
          || (array_search('add', $tasks[$server->machine_name][$id]) === FALSE
              && array_search('clear', $tasks[$server->machine_name][$id]) === FALSE)) {
        $tasks[$server->machine_name][$id][] = 'clear';
        variable_set('process_api_tasks', $tasks);
      }
    }

    $this->reprocess('clear');

    return TRUE;
  }

  /**
   * Deletes this process thread.
   *
   * @return
   *   TRUE on success, FALSE on failure.
   */
  public function delete() {
    parent::delete();
    $this->postDelete();
    return TRUE;
  }

  /**
   * Magic method for determining which fields should be serialized.
   *
   * Don't serialize properties that are basically only caches.
   *
   * @return array
   *   An array of properties to be serialized.
   */
  public function __sleep() {
    $ret = parent::__sleep();
    foreach (array('server_object', 'processors', 'added_properties', 'fulltext_fields') as $field) {
      $i=array_search($field, $ret);
      if ($i!==FALSE) unset($ret[$i]);
    }
    return $ret;
  }

  /**
   * Get the server this thread lies on or the specified one.
   * 
   * @param string $id
   *   The server id, if NULL uses the thread server class.
   *
   * @throws ProcessApiException
   *   If $this->server is set, but no server with that machine name exists.
   *
   * @return ProcessApiServer
   *   The server associated with this thread, or NULL if this thread currently
   *   doesn't lie on a server.
   */
  public function server($server_id=NULL) {
    if (!$server_id) {
      $server_id=$this->server;
    }
    $server_object=NULL;
    if ((!$this->server_object) || ($server_id!=$this->server)) {
      $server_object = $server_id ? process_api_server_load($server_id) : FALSE;
    }
   
    if ($server_id==$this->server) {
      if (!$this->server_object) {
        $this->server_object = $server_object;
      }
      
      if (!$server_object) {
        $server_object = $this->server_object;
      }
    }
    
    if ($server_id && !$server_object) {
      throw new ProcessApiException(t('Unknown server !server specified for thread !name.',
          array('!server' => $server_id, '!name' => $this->machine_name)));
    }
    return $server_object ? $server_object : NULL;
  }

  /**
   * Helper method for ensuring the server object is set up.
   * 
   * @return ProcessApiServer
   */
  public function ensureServer($server_id = NULL) {
    $server_object=$this->server($server_id);
    if (!$server_object) {
      throw new ProcessApiException(t('No server object for !server', array('!server' => $server_id)));    
    }
    return $server_object;
  }  

  /**
   * Helper method for ensuring the proxy object is set up.
   * @param string $server_id
   * @return ProcessApiServiceInterface
   */
  public function ensureProxy($server_id = NULL) {
    if (!$server_id) {
      $server_id=$this->server;
    }
    $proxy=NULL;
    if ((!$this->proxy)||($server_id!=$this->server)) {
      $proxy=$this->ensureServer($server_id)->ensureProxy();
      //TODO proxy fallback if no server...
    }
    if ($server_id==$this->server) {
      if (!$this->proxy) {
        $this->proxy=$proxy;
      }
      if (!$proxy) {
        $proxy=$this->proxy;
      }
    }
    return $proxy;
  }
  
  /**
   * Processes items on this thread. Will return an array of IDs of items that
   * should be marked as processed – i.e. items that were either rejected by a
   * data-alter callback or were successfully processed.
   *
   * @param array $items
   *   An array of entities to process.
   *
   * @return array
   *   An array of the IDs of all items that should be marked as processed.
   */
  public function process(array $items) {
    if (!$this->enabled) {
      throw new ProcessApiException(t("Couldn't thread values on '!name' thread (thread is disabled)", array('!name' => $this->name)));
    }
    if (empty($this->options['fields'])) {
      throw new ProcessApiException(t("Couldn't thread values on '!name' thread (no fields selected)", array('!name' => $this->name)));
    }

    $fields = $this->options['fields'];
    foreach ($fields as $field => $info) {
      if (!$info['processed']) {
        unset($fields[$field]);
      }
      unset($fields[$field]['processed']);
    }
    if (empty($fields)) {
      throw new ProcessApiException(t("Couldn't thread values on '!name' thread (no fields selected)", array('!name' => $this->name)));
    }

    // Marks all items that are rejected by a data-alter callback as processed.
    $ret = array_keys($items);
    $this->dataAlter($items);
    $ret = array_diff($ret, array_keys($items));
    if (!$items) {
      watchdog('process_api', 'Processing the items of type "!type" with the following IDs was rejected by data-alter callbacks: !list.', array('type' => $this->entity_type, '!list' => implode(', ', $ret)));
      return $ret;
    }
    $wrappers = array();
    foreach ($items as $id => $item) {
      $wrappers[$id] = entity_metadata_wrapper($this->entity_type, $item, array('property info alter' => array($this, 'propertyInfoAlter')));
    }

    $items = array();
    foreach ($wrappers as $id => $wrapper) {
      $items[$id] = process_api_extract_fields($wrapper, $fields);
    }
    $this->preprocessThreadItems($items);

    return array_merge($ret, $this->processItems($items));
  }

  /**
   * Calls data alteration hooks for a set of items, according to the thread
   * options. Does nothing if hooks have already been called.
   *
   * @param array $items
   *   An array of items to be altered.
   *
   * @return ProcessApiThread
   *   The called object.
   */
  public function dataAlter(array &$items) {
    if (!empty($this->added_properties)) {
      return $this;
    }
    $this->added_properties = array(
      'process_api_language' => array(
        'label' => t('Item language'),
        'description' => t("A field added by the process framework to let components determine an item's language. Is always processed."),
        'type' => 'string',
      ),
    );
    foreach ($items as &$item) {
      $item->process_api_language = isset($item->language) ? $item->language : LANGUAGE_NONE;
    }

    if (empty($this->options['data_alter_callbacks'])) {
      return;
    }

    foreach ($this->options['data_alter_callbacks'] as $func => $settings) {
      if (empty($settings['status'])) {
        continue;
      }
      if (!is_callable($func)) {
        watchdog('process_api', 'Undefined data alter callback function !function() specified in thread !name', array('!function' => $func, '!name' => $this->name), WATCHDOG_WARNING);
        continue;
      }
      $ret = $func($this, $items);
      if (is_array($ret)) {
        $this->added_properties += $ret;
      }
    }

    return $this;
  }

  /**
   * Property info alter callback that adds the infos of the properties added by
   * data alter callbacks.
   *
   * @param EntityMetadataWrapper $wrapper
   *   The wrapped data.
   * @param $property_info
   *   The original property info.
   *
   * @return array
   *   The altered property info.
   */
  public function propertyInfoAlter(EntityMetadataWrapper $wrapper, array $property_info) {
    // Overwrite the existing properties with the list of properties including
    // all fields regardless of the used bundle.
    $property_info['properties'] = entity_get_all_property_info($wrapper->type());

    if (!empty($this->added_properties)) {
      $property_info['properties'] += $this->added_properties;
    }
    return $property_info;
  }

  /**
   * Fills the $processors array for use by the pre-/postprocessing functions.
   *
   * @return ProcessApiThread
   *   The called object.
   */
  protected function prepareProcessors() {
    if (isset($this->processors)) {
      return $this;
    }

    $this->processors = array();
    if (empty($this->options['processors'])) {
      return $this;
    }
    $processor_settings = $this->options['processors'];
    $infos = process_api_get_processors();

    foreach ($processor_settings as $id => $settings) {
      if (empty($settings['status'])) {
        continue;
      }
      if (empty($infos[$id]) || !class_exists($infos[$id]['class'])) {
        watchdog('process_api', 'Undefined processor !class specified in thread !name', array('!class' => $id, '!name' => $this->name), WATCHDOG_WARNING);
        continue;
      }
      $class = $infos[$id]['class'];
      $processor = new $class($this, isset($settings['settings']) ? $settings['settings'] : array());
      if (!($processor instanceof ProcessApiProcessorInterface)) {
        watchdog('process_api', 'Unknown processor class !class specified for processor !name', array('!class' => $class, '!name' => $id), WATCHDOG_WARNING);
        continue;
      }

      $this->processors[$id] = $processor;
    }
    return $this;
  }

  /**
   * Preprocess data items for processing. Data added by data alter callbacks will
   * be available on the items.
   *
   * Typically, a preprocessor will execute its preprocessing (e.g. stemming,
   * n-grams, word splitting, stripping stop words, etc.) only on the items'
   * fulltext fields. Other fields should usually be left untouched.
   *
   * @param array $items
   *   An array of items to be preprocessed for processing.
   *
   * @return ProcessApiThread
   *   The called object.
   */
  public function preprocessThreadItems(array &$items) {
    $this->prepareProcessors();
    foreach ($this->processors as $processor) {
      $processor->preprocessThreadItems($items);
    }
    return $this;
  }

  /**
   * Convenience method for getting all of this thread' fulltext fields.
   *
   * @param boolean $only_processed
   *   If set to TRUE, only the processed fulltext fields will be returned.
   *
   * @return array
   *   An array containing all (or all processed) fulltext fields defined for this
   *   thread.
   */
  public function getFulltextFields($only_processed = TRUE) {
    
    $i = $only_processed ? 1 : 0;
    if (!isset($this->fulltext_fields[$i])) {
      $this->fulltext_fields[$i] = array();
      if (empty($this->options['fields'])) {
        return array();
      }
      foreach ($this->options['fields'] as $key => $field) {       
        if (process_api_is_text_type($field['type']) && (!$only_processed || $field['processed'])) {
          $this->fulltext_fields[$i][] = $key;
        }
      }
    }
    return $this->fulltext_fields[$i];
  }

  // server's proxy methods

  /**
   * If the service class defines additional methods, not specified in the
   * ProcessApiServiceInterface interface, then they are called via this magic
   * method.
   * 
   * The method is prefixed with thread.
   * The thread object ($this) is added as first argument.
   * ie. foo() becomes threadFoo($thread)
   */
  public function __call($name, $arguments = array()) {
    array_unshift($arguments, $this);
    return call_user_func_array(array($this->ensureProxy(), 'thread' . ucfirst($name)), $arguments);
  }
  
  // hard coded proxy methods with 'by reference' params or special behaviour

  public function fieldConfigurationForm($header_id, array $field_info, array $form, array &$form_state) {
    return $this->ensureProxy()->threadFieldConfigurationForm($this, $header_id, $field_info, $form, $form_state);
  }
  
  public function configurationForm(array $form, array &$form_state, $server_id = NULL) {
    return $this->ensureProxy($server_id)->threadConfigurationForm($this, $form, $form_state);
  }

  public function configurationFormValidate(array $form, array $values, array &$form_state, $server_id = NULL) {
    return $this->ensureProxy($server_id)->threadConfigurationFormValidate($this, $form, $values, $form_state);
  }

  public function configurationFormSubmit(array $form, array $values, array &$form_state, $server_id = NULL) {
    return $this->ensureProxy($server_id)->threadConfigurationFormSubmit($this, $form, $values, $form_state);
  }
  
  /**
   * Trigger function.
   * @see applyChangeTriggers()
   * 
   * @param string $old
   * @param string $new
   * @param string $tag
   */
  protected function changeTriggerServer($old, $new, $tag) {
    
    // it should be no more old since server can't be modified now
    if ($old) {
      $old_server = $this->server($old);
      if ($old_server->enabled) {
        $old_server->removeThread($this);
      }
      else {
        // TODO why postpone
        $tasks = variable_get('process_api_tasks', array());
        // When we add or remove a thread, we can ignore all other tasks.
        $tasks[$old_server->machine_name][$this->machine_name] = array('remove');
        variable_set('process_api_tasks', $tasks);  // TODO ugly
      }
    }

    if ($new) {
      $new_server = $this->server();
      // If the server is disabled, we save the task. Otherwise we call addThread() after saving the entry.
      if ($new_server->enabled) {
        $new_server->addThread($this);
      }
      else {
        // TODO why postpone
        $this->enabled = FALSE;
        $tasks = variable_get('process_api_tasks', array());
        // When we add or remove a thread, we can ignore all other tasks.
        $tasks[$new_server->machine_name][$this->machine_name] = array('add');
        variable_set('process_api_tasks', $tasks); // TODO ugly
      }
    }
    else {
      $this->enabled = FALSE;
    }

    $this->needReprocess('server');

    return TRUE;
  }
  
  /**
   * Trigger function.
   * @see applyChangeTriggers()
   * 
   * @param string $old
   * @param string $new
   * @param string $tag
   */
  protected function changeTriggerOptions($old, $new, $tag) {
    switch ($tag) {
      case 'post_update':
        // Determine whether the processed fields were changed for later calling appropriate hooks.
        if ( ( isset($old['fields']) && isset($new['fields']) && $old['fields'] != $new['fields'] ) 
          || ( isset($old['fields']) != isset($new['fields']) ) ) { // new from before
            $this->needReprocess('fields');
        }
        //TODO manage other options
        break;
    }
  }
}
