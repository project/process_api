<?php

/**
 * @file
 * Service classes.
 */

/**
 * Interface defining the methods process services have to implement.
 *
 * Before a service object is used, the corresponding server's data will be read
 * from the database (see ProcessApiServiceAbstract for a list of fields).
 */
interface ProcessApiServiceInterface {

  /**
   * Constructor for a service class, setting the server configuration used with
   * this service.
   *
   * @param ProcessApiServer $server
   *   The server object for this service.
   */
  public function __construct(ProcessApiServer $server);

  /**
   * Server form callback. Might be called on an uninitialized object - in this case,
   * the form is for configuring a newly created server.
   *
   * @return array
   *   A form array for setting service-specific options.
   */
  public function configurationForm(array $form, array &$form_state);

  /**
   * Server validation callback for the form returned by configurationForm().
   *
   * $form_state['server'] will contain the server that is created or edited.
   * Use form_error() to flag errors on form elements.
   *
   * @param array $form
   *   The full form.
   * @param array $values
   *   The part of the $form_state['values'] array corresponding to this form.
   * @param array $form_state
   *   The complete form state.
   */
  public function configurationFormValidate(array $form, array $values, array &$form_state);

  /**
   * Server submit callback for the form returned by configurationForm().
   *
   * This method should set the options of this service' server according to
   * $values.
   *
   * @param array $form
   *   The full form.
   * @param array $values
   *   The part of the $form_state['values'] array corresponding to this form.
   * @param array $form_state
   *   The complete form state.
   */
  public function configurationFormSubmit(array $form, array $values, array &$form_state);

  /**
   * Return informations on additional configuration in fields form.
   * 
   * @param ProcessApiThread $thread
   *   array(
   *     'header1_id' => 'Header 1',
   *     'header2_id' => 'Header 2',
   *   )
   */
  public function threadFieldsConfigurationHeaders(ProcessApiThread $thread);

  /**
   * Return a form element for given field and header.
   * @see threadFieldsConfigurationHeaders()
   * 
   * @param ProcessApiThread $thread
   * @param string $header_id
   * 
   * @return Array
   *   A form element
   */
  public function threadFieldConfigurationForm(ProcessApiThread $thread, $header_id, array $field_info, array $form, array &$form_state);
  
  /**
   * Thread form callback. Might be called on an uninitialized object - in this case,
   * the form is for configuring a newly created thread.
   *
   * @param ProcessApiThread $thread
   *   The thread object
   *   
   * @return array
   *   A form array for setting service-specific options.
   */
  public function threadConfigurationForm(ProcessApiThread $thread, array $form, array &$form_state);
  
  /**
   * Thread validation callback for the form returned by threadConfigurationForm().
   *
   * $form_state['thread'] will contain the thread that is created or edited.
   * Use form_error() to flag errors on form elements.
   *
   * @param ProcessApiThread $thread
   *   The thread object
   * @param array $form
   *   The form returned by threadConfigurationForm().
   * @param array $values
   *   The part of the $form_state['values'] array corresponding to this form.
   * @param array $form_state
   *   The complete form state.
   */
  public function threadConfigurationFormValidate(ProcessApiThread $thread, array $form, array $values, array &$form_state);

  /**
   * Thread submit callback for the form returned by threadConfigurationForm().
   *
   * This method should set the options of this service' thread according to
   * $values.
   *
   * @param ProcessApiThread $thread
   *   The thread object
   * @param array $form
   *   The form returned by threadConfigurationForm().
   * @param array $values
   *   The part of the $form_state['values'] array corresponding to this form.
   * @param array $form_state
   *   The complete form state.
   */
  public function threadConfigurationFormSubmit(ProcessApiThread $thread, array $form, array $values, array &$form_state);
  
  
  /**
   * Determines whether this service class implementation supports a given
   * feature. Features are optional extensions to Process API functionality and
   * usually defined and used by third-party modules.
   * Currently, the only feature specified directly in the process_api project is
   * "process_api_facets", defined by the module of the same name.
   *
   * @param string $feature
   *   The name of the optional feature.
   *
   * @return boolean
   *   TRUE if this service knows and supports the specified feature. FALSE
   *   otherwise.
   */
  public function supportsFeature($feature);

  /**
   * View this server's settings. Output can be HTML or a render array, a <dl>
   * listing all relevant settings is preferred.
   */
  public function viewSettings();

  /**
   * View this thread's settings. Output can be HTML or a render array, a <dl>
   * listing all relevant settings is preferred.
   * 
   * @param ProcessApiThread $thread
   */
  public function threadViewSettings(ProcessApiThread $thread);
  
  /**
   * Called once, when the server is first created. Allows it to set up its
   * necessary infrastructure.
   */
  public function postCreate();

  /**
   * Notifies this server that its fields are about to be updated. Fields set on
   * this object will still show the old values to allow comparison.
   *
   * @param array $fields
   *   The fields that are about to change, mapped to their values.
   *
   * @return
   *   TRUE, if the update requires reprocessing of all content on the server.
   */
  public function postUpdate(array $fields);

  /**
   * Notifies this server that it has been re-enabled and allows the object to
   * react appropriately.
   */
  public function postEnable();

  /**
   * Notifies this server that it has been disabled and allows the object to
   * react appropriately.
   */
  public function postDisable();

  /**
   * Notifies this server that it is about to be deleted from the database and
   * should therefore clean up, if appropriate.
   */
  public function preDelete();

  /**
   * Add a new thread to this server.
   *
   * If the thread was already added to the server, the object should treat this
   * as if removeThread() and then addThread() were called.
   *
   * @param ProcessApiThread $thread
   *   The thread to add.
   */
  public function addThread(ProcessApiThread $thread);

  /**
   * Notify the server that the processed field settings for the thread have
   * changed.
   * If any user action is necessary as a result of this, the method should
   * use drupal_set_message() to notify the user.
   *
   * @param ProcessApiThread $thread
   *   The updated thread.
   *
   * @return
   *   TRUE, if this change affected the server in any way that forces it to
   *   re-process the content. FALSE otherwise.
   */
  public function fieldsUpdated(ProcessApiThread $thread);

  /**
   * Remove a thread from this server.
   *
   * This might mean that the thread has been deleted, or reassigned to a
   * different server. If you need to distinguish between these cases, inspect
   * $thread->server.
   *
   * If the thread wasn't added to the server, the method call should be ignored.
   *
   * @param $thread
   *   Either an object representing the thread to remove, or its machine name
   *   (if the thread was completely deleted).
   */
  public function removeThread($thread);

  /**
   * Process a single item.
   * 
   * @param ProcessApiThread $thread
   * @param interger $id
   *   The main object id.
   * @param array $item
   *   The item to process.
   * @param mixed &$context
   *   A context for the process. threadProcessItem() is called in a loop surrounded by threadBeginProcess() and threadEndProcess() so you can optionaly define a context.
   * 
   * Array
   * (
   *     [<id>] => Array
   *         (
   *             [name]    => 'Field name',
   *             [value]   => 'Field value',
   *             [type]    => 'type',
   *             [wrapper] =>  wrapper object,
   *             // there is also one entry for each header defined by threadFieldsConfigurationHeaders()
   *             [<header_id>] => <conf value>
   *         ),
   *     ...
   * )
   * 
   * @return boolean
   *  TRUE means entity is successfully processed
   *  FALSE means it's not and will be processed again
   */
  public function threadProcessItem(ProcessApiThread $thread, $id, array $item, &$context);
  
  /**
   * Allow process engines to do some init stuff. 
   * 
   * @param ProcessApiThread $thread
   * @param mixed &$context
   *   A context to pass to threadProcessItem()
   */
  public function threadBeginProcess(ProcessApiThread $thread, &$context);

  /**
   * Allow process engines to do some cleanup stuff. 
   * 
   * @param ProcessApiThread $thread
   * @param mixed &$context
   */
  public function threadEndProcess(ProcessApiThread $thread, &$context);
  
  /**
   * Delete items from a thread on this server.
   *
   * Might be either used to delete some items (given by their ids) from a
   * specified thread, or all items from that thread, or all items from all
   * threads on this server.
   *
   * @param $ids
   *   Either an array containing the ids of the items that should be deleted,
   *   or 'all' if all items should be deleted. Other formats might be
   *   recognized by implementing classes, but these are not standardized.
   * @param ProcessApiThread $thread
   *   The thread from which items should be deleted, or NULL if all threads on
   *   this server should be cleared (then, $ids has to be 'all').
   */
  public function deleteItems($ids = 'all', ProcessApiThread $thread = NULL);

  /**
   * Called in hook_entity_insert() foreach thread.
   * 
   * @param ProcessApiThread $thread
   * @param stdClass $entity
   * @param string $type
   * @param integer $id
   */
  public function threadEntityInsert(ProcessApiThread $thread, $entity, $type, $id);
  
  /**
   * Called in hook_entity_update() foreach thread.
   * 
   * @param ProcessApiThread $thread
   * @param stdClass $entity
   * @param string $type
   * @param integer $id
   */
  public function threadEntityUpdate(ProcessApiThread $thread, $entity, $type, $id);

  /**
   * Returns a list of at most $limit items that need to be processed for the
   * specified thread.
   *
   * @param ProcessApiThread $thread
   *   The thread for which items should be retrieved.
   * @param $limit
   *   The maximum number of items to retrieve. -1 means no limit.
   *
   * @return array
   *   An array of items (entities) that need to be processed.
   */
  public function threadGetItemsToProcess(ProcessApiThread $thread, $limit = -1);
  
  /**
   * Get the list of supported entity types.
   *
   * @see entity_get_info()
   * @return array
   *   The list of entity types supported by this service in entity_get_info() format.
   */
  public function getSupportedEntityTypes();
  
}

/**
 * Abstract class with generic implementation of most service methods.
 */
abstract class ProcessApiServiceAbstract implements ProcessApiServiceInterface {

  /**
   * @var ProcessApiServer
   */
  protected $server;

  /**
   * Direct reference to the server's $options property.
   *
   * @var array
   */
  protected $options = array();

  /**
   * Constructor for a service class, setting the server configuration used with
   * this service.
   *
   * @param ProcessApiServer $server
   *   The server object for this service.
   */
  public function __construct(ProcessApiServer $server) {
    $this->server = $server;
    $this->options = &$server->options;
  }

  /**
   * Default implementation.
   */
  public function configurationForm(array $form, array &$form_state) {
    return array();
  }

  /**
   * Default implementation.
   */
  public function configurationFormValidate(array $form, array $values, array &$form_state) {
    return;
  }

  /**
   * Default implementation.
   */
  public function configurationFormSubmit(array $form, array $values, array &$form_state) {
    if (!isset($this->server->options['custom'])) $this->server->options['custom']=array();
    $this->server->options['custom'] = array_merge($this->server->options['custom'], $values);
  }

  /**
   * Default implementation.
   */
  public function threadConfigurationForm(ProcessApiThread $thread, array $form, array &$form_state) {
    return array();
  }

  /**
   * Default implementation.
   * @see ProcessApiServiceInterface::threadConfigurationFormValidate()
   */
  public function threadConfigurationFormValidate(ProcessApiThread $thread, array $form, array $values, array &$form_state) {
    return;
  }

  /**
   * Default implementation.
   * @see ProcessApiServiceInterface::threadConfigurationFormSubmit()
   */ 
  public function threadConfigurationFormSubmit(ProcessApiThread $thread, array $form, array $values, array &$form_state) {
    if (!isset($thread->options['custom'])) $thread->options['custom']=array();
    $thread->options['custom'] = array_merge($thread->options['custom'], $values);
  }
  
  /**
   * (Default implementation.
   * @see ProcessApiServiceInterface::threadFieldsConfigurationHeaders()
   */
  public function threadFieldsConfigurationHeaders(ProcessApiThread $thread) {
    return array();
  }

  /**
   * Default implementation.
   * @see ProcessApiServiceInterface::threadFieldConfigurationForm()
   */
  public function threadFieldConfigurationForm(ProcessApiThread $thread, $header_id, array $field_info, array $form, array &$form_state) {
    return array();
  }
  
  /**
   * Determines whether this service class implementation supports a given
   * feature. Features are optional extensions to Process API functionality and
   * usually defined and used by third-party modules.
   * Currently, the only feature specified directly in the process_api project is
   * "process_api_facets", defined by the module of the same name.
   *
   * @param string $feature
   *   The name of the optional feature.
   *
   * @return boolean
   *   TRUE if this service knows and supports the specified feature. FALSE
   *   otherwise.
   */
  public function supportsFeature($feature) {
    return FALSE;
  }

  /**
   * View this server's settings. Output can be HTML or a render array, a <dl>
   * listing all relevant settings is preferred.
   *
   * The default implementation does a crude output as a definition list, with
   * option names taken from the configuration form.
   */
  public function viewSettings() {
    $output = '';
    $form = $form_state = array();
    $option_form = $this->configurationForm($form, $form_state);

    foreach ($option_form as $key => $element) {
      if (isset($element['#title']) && isset($this->server->options['custom'][$key])) {
        $output .= '<dt>' . check_plain($element['#title']) . '</dt>' . "\n";
        switch ($element['#type']) {
          case 'password':
            $output .= '<dd>' . str_repeat('*', strlen($this->server->options['custom'][$key])) . '</dd>' . "\n";
            break;
          case 'select':
            $output .= '<dd>' . $element['#options'][$this->server->options['custom'][$key]] . '</dd>' . "\n";
            break;
          case 'checkbox':
            $output .= '<dd>' . ($this->server->options['custom'][$key]?t('Yes'):t('No')) . '</dd>' . "\n";
            break;
          default:
            $output .= '<dd>' . nl2br(check_plain(print_r($this->server->options['custom'][$key], TRUE))) . '</dd>' . "\n";
        }
      }
    }

    return $output ? "<dl>\n$output</dl>" : '';
  }
  
  /**
   * Default implementation.
   * @see ProcessApiServiceInterface::threadViewSettings()
   */
  public function threadViewSettings(ProcessApiThread $thread) {
    
    $output = '';
    $form = $form_state = array();
    $option_form = $this->threadConfigurationForm($thread, $form, $form_state);

    foreach ($option_form as $key => $element) {
      if (isset($element['#title']) && isset($thread->options['custom'][$key])) {
        $output .= '<dt>' . check_plain($element['#title']) . '</dt>' . "\n";
        switch ($element['#type']) {
          case 'password':
            $output .= '<dd>' . str_repeat('*', strlen($thread->options['custom'][$key])) . '</dd>' . "\n";
            break;
          case 'select':
            $output .= '<dd>' . $element['#options'][$thread->options['custom'][$key]] . '</dd>' . "\n";
            break;
          case 'checkbox':
            $output .= '<dd>' . ($thread->options['custom'][$key]?t('Yes'):t('No')) . '</dd>' . "\n";
            break;
          default:
            $output .= '<dd>' . nl2br(check_plain(print_r($thread->options['custom'][$key], TRUE))) . '</dd>' . "\n";
        }
      }
    }
    
    return $output ? "<dl>\n$output</dl>" : '';
  }  

  /**
   * Called once, when the server is first created. Allows it to set up its
   * necessary infrastructure.
   *
   * Does nothing, by default.
   */
  public function postCreate() {
    return;
  }

  /**
   * Notifies this server that its fields are about to be updated. Fields set on
   * this object will still show the old values to allow comparison.
   *
   * Does nothing, by default.
   *
   * @param array $fields
   *   The fields that are about to change, mapped to their values.
   *
   * @return
   *   TRUE, if the update requires reprocessing of all content on the server.
   */
  public function postUpdate(array $fields) {
    return FALSE;
  }

  /**
   * Notifies this server that it has been re-enabled and allows the object to
   * react appropriately.
   *
   * Does nothing, by default.
   */
  public function postEnable() {
    return;
  }

  /**
   * Notifies this server that it has been disabled and allows the object to
   * react appropriately.
   *
   * Does nothing, by default.
   */
  public function postDisable() {
    return;
  }

  /**
   * Notifies this server that it is about to be deleted from the database and
   * should therefore clean up, if appropriate.
   *
   * By default, deletes all threads from this server.
   */
  public function preDelete() {
    $threads = process_api_thread_load_multiple(FALSE, array('server' => $this->server->machine_name), FALSE, TRUE);
    foreach ($threads as $thread) {
      $this->removeThread($thread);
    }
  }

  /**
   * Add a new thread to this server.
   *
   * Does nothing, by default.
   *
   * @param ProcessApiThread $thread
   *   The thread to add.
   */
  public function addThread(ProcessApiThread $thread) {
    return;
  }

  /**
   * Notify the server that the processed field settings for the thread have
   * changed.
   * If any user action is necessary as a result of this, the method should
   * use drupal_set_message() to notify the user.
   *
   * @param ProcessApiThread $thread
   *   The updated thread.
   *
   * @return
   *   TRUE, if this change affected the server in any way that forces it to
   *   re-process the content. FALSE otherwise.
   */
  public function fieldsUpdated(ProcessApiThread $thread) {
    return FALSE;
  }

  /**
   * Remove a thread from this server.
   *
   * This might mean that the thread has been deleted, or reassigned to a
   * different server. If you need to distinguish between these cases, inspect
   * $thread->server.
   *
   * By default, removes all items from that thread.
   *
   * @param $thread
   *   Either an object representing the thread to remove, or its machine name
   *   (if the thread was completely deleted).
   */
  public function removeThread($thread) {
    $this->deleteItems('all', $thread);
  }

  public function threadBeginProcess(ProcessApiThread $thread, &$context) {
    
  }
  
  public function threadEndProcess(ProcessApiThread $thread, &$context) {
  
  }

    /**
   * Process the specified items.
   *
   * @param ProcessApiThread $thread
   *   The process thread for which items should be processed.
   * @param array $items
   *   An array of items to be processed, keyed by their id. The values are
   *   associative arrays of the fields to be stored, where each field is an
   *   array with the following keys:
   *   - type: One of the data types recognized by the Process API, or the
   *     special type "tokens" for fulltext fields.
   *   - original_type: The original type of the property as defined through a
   *     hook_entity_property_info().
   *   - value: The value to process.
   *
   *   The special field "process_api_language" contains the item's language and
   *   should always be processed.
   *
   *   The value of fields with the "tokens" type is an array of tokens. Each
   *   token is an array containing the following keys:
   *   - value: The word that the token represents.
   *   - score: A score for the importance of that word.
   *
   * @throws ProcessApiException
   *   If processing was prevented by a fundamental configuration error.
   *
   * @return array
   *   An array of the ids of all items that were successfully processed.
   */
  public function threadProcessItems(ProcessApiThread $thread, array $items) {
    $ret=array();
    $this->threadBeginProcess($thread, $context);
    foreach ($items as $id => $item) {
      // try avoid auto update loop in hook_entity_update()
      process_api_add_semaphore($thread->machine_name, $id);
      $res=$this->threadProcessItem($thread, $id, $item, $context);
      process_api_remove_semaphore($thread->machine_name, $id);
      if ($res) {
        $this->threadSetItemProcessed($thread, $id);
        $ret[]=$id;
      }
    }
    $this->threadEndProcess($thread, $context);
    return $ret;
  }

  /**
   * Flag the given item as proccecessed (or not).
   * 
   * @param ProcessApiThread $thread
   * @param integer $id
   * @param integer $changed timestamp or 0 for processed
   * 
   */
  public function threadSetItemProcessed(ProcessApiThread $thread, $id, $changed=0) {
    db_merge('process_api_item')
      ->key(
        array(
          'thread_id',
          'item_id'),
        array(
          $thread->machine_name,
          $id,
        )      
      )
      ->fields(array(
        'changed' => $changed,
      ))
      ->execute();
  }
  
    /**
   * Called in hook_entity_insert() foreach thread.
   * Default implementation marks the corresponding item to be processed and try to process it if thread is "live".
   * 
   * @param ProcessApiThread $thread
   * @param stdClass $entity
   * @param string $type
   * @param integer $id
   */
  public function threadEntityInsert(ProcessApiThread $thread, $entity, $type, $id) {
    $this->threadEntityInsertUpdate($thread, $entity, $type, $id);
  }
  
  /**
   * Called in hook_entity_update() foreach thread.
   * Default implementation marks the corresponding item to be re-processed and try to process it if thread is "live".
   * 
   * @param ProcessApiThread $thread
   * @param stdClass $entity
   * @param string $type
   * @param integer $id
   */
  public function threadEntityUpdate(ProcessApiThread $thread, $entity, $type, $id) {
    $this->threadEntityInsertUpdate($thread, $entity, $type, $id);
  }

  protected function threadEntityInsertUpdate(ProcessApiThread $thread, $entity, $type, $id) {
    db_merge('process_api_item')
      ->key(
        array(
          'thread_id',
          'item_id'),
        array(
          $thread->machine_name,
          $id,
        )      
      )
      ->fields(array(
        'changed' => time(),
      ))
      ->execute();
    
    if ($thread->enabled && isset($thread->options['live']) && $thread->options['live']) {
      try {
        $thread->process(array($id => $entity));
      }
      catch (ProcessApiException $e) {
        watchdog('process_api', 'An error occurred while trying to process items on thread !name: !message',
            array('!name' => $thread->machine_name, '!message' => $e->getMessage()), WATCHDOG_ERROR);
      }
    }
  }
  
  /**
   * Default implementation.
   * @see ProcessApiServiceInterface::threadGetItemsToProcess()
   */
  public function threadGetItemsToProcess(ProcessApiThread $thread, $limit = -1) {
    if ($limit == 0) {
      return array();
    }
    $select = db_select('process_api_item', 'i');
    $select->addField('i', 'item_id');
    $select->condition('thread_id', $thread->machine_name);
    $select->condition('changed', 0, '<>');
    $select->orderBy('changed', 'ASC');
    if ($limit > 0) {
      $select->range(0, $limit);
    }

    $ids = $select->execute()->fetchCol();
    return entity_load($thread->entity_type, $ids);
  }
  
  /**
   * Default implementation.
   * @see ProcessApiServiceInterface::deleteItems()
   */
  public function deleteItems($ids = 'all', ProcessApiThread $thread = NULL) {
    //does nothing
  }
  
  /**
   * Default implementation.
   * Return all entity types.
   * @see ProcessApiServiceInterface::getSupportedEntityTypes()
   */
  public function getSupportedEntityTypes() {
    return entity_get_info();
  }
}

